return {
    bufName  = nil,
    bufNr    = nil,
    bufType  = nil,
    winID    = nil,
    winIDtbl = nil,
    bufNrTbl = nil,

    newSplitLastBufNr = nil,
    lastClosedFilePath = nil
}
