return function()
    require("nvim-treesitter.configs").setup{
        -- ensure_installed = "maintained",
        ensure_installed = {
            "c", "cpp", "cmake", "lua", "json", "toml",
            "python", "bash", "fish", "ruby", "regex", "css", "html",
            "go", "javascript", "rust", "vue", "c_sharp", "typescript",
            "comment", "query", "yaml"
        },
        auto_install = true,
        sync_install = true,
        highlight = {
            enable = true,
            disable = function(lang, buf)
                -- Filetype Exclude
                if vim.o.filetype == "help" then return true end

                local max_filesize = 100 * 1024 -- 100 KB
                local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
                if ok and stats and stats.size > max_filesize then
                    return true
                end
            end,
            additional_vim_regex_highlighting = false
        },
        incremental_selection = {
            enable  = true,
            keymaps = {
                init_selection    = "gnn",
                node_incremental  = "grn",
                node_decremental  = "grm",
                scope_incremental = "grc",
            },
        },
        matchup = {
            enable  = true,
            disable = {"help"}
        },
    }

    map("n",        [[<A-S-a>]], [[gnn]], "Expand selection")
    map("x",        [[<A-S-a>]], [[grc]], "Expand selection")
    map({"n", "x"}, [[<A-S-s>]], [[grm]], "Shirnk selection")
end
