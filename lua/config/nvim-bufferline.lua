local M = {}


M.cycleBuf = function(CMD, fallbackCMD)
    local bufNr = vim.api.nvim_get_current_buf()
    vim.cmd(CMD)
    if vim.api.nvim_get_current_buf() == bufNr then vim.cmd(fallbackCMD) end
end


M.config = function()

require("bufferline").setup {
    options = {
        numbers = "none",
        close_command        = "bdelete! %d",  -- can be a string | function, see "Mouse actions"
        right_mouse_command  = "bdelete! %d",  -- can be a string | function, see "Mouse actions"
        left_mouse_command   = "buffer %d",    -- can be a string | function, see "Mouse actions"
        middle_mouse_command = nil,            -- can be a string | function, see "Mouse actions"
        -- NOTE: this plugin is designed with this icon in mind,
        -- and so changing this is NOT recommended, this is intended
        -- as an escape hatch for people who cannot bear it for whatever reason
        -- indicator_icon     = "▎",
        buffer_close_icon  = "",
        modified_icon      = "●",
        close_icon         = "",
        left_trunc_marker  = "",
        right_trunc_marker = "",
        --- name_formatter can be used to change the buffer"s label in the bufferline.
        --- Please note some names can/will break the
        --- bufferline so use this at your discretion knowing that it has
        --- some limitations that will *NOT* be fixed.
        name_formatter = function(buf)  -- buf contains a "name", "path" and "bufnr"
        -- remove extension from markdown files for example
            if buf.name:match("%.md") then
                return vim.fn.fnamemodify(buf.name, ":t:r")
            end
        end,
        max_name_length   = 18,
        max_prefix_length = 15, -- prefix used when a buffer is de-duplicated
        tab_size          = 18,
        diagnostics       = false,
        diagnostics_update_in_insert = false,
        diagnostics_indicator = function(count, level, diagnostics_dict, context)
            return "("..count..")"
            end,
        -- NOTE: this will be called a lot so don"t do any heavy processing here
        custom_filter = function(buf_number)
            -- filter out filetypes you don"t want to see
            if vim.bo[buf_number].filetype ~= "help" then
                return true
            end
            -- filter out by buffer name
            if vim.fn.bufname(buf_number) ~= "nowrite" then
                return true
            end
            -- filter out based on arbitrary rules
            -- e.g. filter out vim wiki buffer from tabline in your work repo
            -- if vim.fn.getcwd() == "<work-repo>" and vim.bo[buf_number].filetype ~= "wiki" then
                -- return true
            -- end
        end,
        offsets = {{filetype = "NvimTree", text = "Nvim Tree", text_align = "left", highlight = "BufferLineBufferSelected"}},
        show_buffer_icons       = true, -- disable filetype icons for buffers
        show_buffer_close_icons = true,
        show_close_icon         = false,
        show_tab_indicators     = true,
        persist_buffer_sort     = true, -- whether or not custom sorted buffers should persist
        -- can also be a table containing 2 custom separators
        -- [focused and unfocused]. eg: { "|", "|" }
        separator_style        = {"", ""},
        enforce_regular_tabs   = false,
        always_show_bufferline = true,
        sort_by                = "id"
    },

    highlights = { -- {{{
        fill = {
            fg = "#eeeeee",
            bg = "#3B4252",
        },
        background = {
            fg = "#66738e",
            bg = "#3B4252",
        },
        tab = {
            fg = "#66738e",
            bg = "#3B4252",
        },
        tab_selected = {
            fg = "#D8DEE9",
            bg = "#4C566A",
        },
        tab_close = {
            fg = "#66738e",
            bg = "#3B4252",
        },
        close_button = {
            fg = "#66738e",
            bg = "#3B4252",
        },
        close_button_visible = {
            fg = "#66738e",
            bg = "#3B4252",
        },
        close_button_selected = {
            fg = "#D8DEE9",
            bg = "#4C566A",
        },
        buffer_visible = {
            fg = "#66738e",
            bg = "#3B4252",
        },
        buffer_selected = {
            fg = "#D8DEE9",
            bg = "#4C566A",
            bold = true,
            italic = false,
        },
        numbers = {
            fg = "#66738e",
            bg = "#3B4252",
        },
        numbers_visible = {
            fg = "#66738e",
            bg = "#3B4252",
        },
        numbers_selected = {
            fg = "#D8DEE9",
            bg = "#4C566A",
            bold = true,
            italic = false,
        },
        modified = {
            fg = "#9B8473",
            bg = "#3B4252"
        },
        modified_visible = {
            fg = "#9B8473",
            bg = "#3B4252",
        },
        modified_selected = {
            fg = "#EBCB8B",
            bg = "#4C566A"
        },
        duplicate_selected = {
            italic = true,
            fg = "#9B8473",
            bg = "#4C566A"
        },
        duplicate_visible = {
            italic = true,
            fg = "#9B8473",
            bg = "#4C566A"
        },
        duplicate = {
            italic = true,
            fg = "#9B8473",
            bg = "#4C566A"
        },
        separator_selected = {
            fg = "#4C566A",
            bg = "#4C566A"
        },
        separator_visible = {
            fg = "#3B4252",
            bg = "#3B4252"
        },
        separator = {
            fg = "#3B4252",
            bg = "#3B4252"
        },
        indicator_visible = {
            fg = "#3B4252",
            bg = "#3B4252"
        },
        indicator_selected = {
            fg = "#88C0D0",
            bg = "#4C566A"
        },
        pick_selected = {
            fg = "#4C566A",
            bg = "#4C566A",
        },
        pick_visible = {
            fg = "#ED427C",
            bg = "#3B4252",
            bold = true
        },
        pick = {
            bold = true,
            fg   = "#ED427C",
            bg   = "#3B4252",
        }

        -- diagnostic = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
        -- },
        -- diagnostic_visible = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
        -- },
        -- diagnostic_selected = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            --  = "bold,italic"
        -- },
        -- info = {
            -- fg = <color-value-here>,
            -- sp = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- info_visible = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- info_selected = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            -- bold = true,
            -- italic = true,
            -- sp = <color-value-here>
        -- },
        -- info_diagnostic = {
            -- fg = <color-value-here>,
            -- sp = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- info_diagnostic_visible = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- info_diagnostic_selected = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            -- bold = true,
            -- italic = true,
            -- sp = <color-value-here>
        -- },
        -- warning = {
            -- fg = <color-value-here>,
            -- sp = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- warning_visible = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- warning_selected = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            -- bold = true,
            -- italic = true,
            -- sp = <color-value-here>
        -- },
        -- warning_diagnostic = {
            -- fg = <color-value-here>,
            -- sp = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- warning_diagnostic_visible = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- warning_diagnostic_selected = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            -- bold = true,
            -- italic = true,
            -- sp = warning_diagnostic_fg
        -- },
        -- error = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            -- sp = <color-value-here>
        -- },
        -- error_visible = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- error_selected = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            -- bold = true,
            -- italic = true,
            -- sp = <color-value-here>
        -- },
        -- error_diagnostic = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            -- sp = <color-value-here>
        -- },
        -- error_diagnostic_visible = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>
        -- },
        -- error_diagnostic_selected = {
            -- fg = <color-value-here>,
            -- bg = <color-value-here>,
            -- bold = true,
            -- italic = true,
            -- sp = <color-value-here>
        -- },
    } -- }}}
}

-- map("n", [[<leader>b]], [[:<C-u>BufferLinePick<CR>]], {"silent"})
map("n", [[<A-1>]], [[:lua require("bufferline").go_to_buffer(1)<CR>]], {"silent"}, "Go to buffer 1")
map("n", [[<A-2>]], [[:lua require("bufferline").go_to_buffer(2)<CR>]], {"silent"}, "Go to buffer 2")
map("n", [[<A-3>]], [[:lua require("bufferline").go_to_buffer(3)<CR>]], {"silent"}, "Go to buffer 3")
map("n", [[<A-4>]], [[:lua require("bufferline").go_to_buffer(4)<CR>]], {"silent"}, "Go to buffer 4")
map("n", [[<A-5>]], [[:lua require("bufferline").go_to_buffer(5)<CR>]], {"silent"}, "Go to buffer 5")
map("n", [[<A-6>]], [[:lua require("bufferline").go_to_buffer(6)<CR>]], {"silent"}, "Go to buffer 6")
map("n", [[<A-7>]], [[:lua require("bufferline").go_to_buffer(7)<CR>]], {"silent"}, "Go to buffer 7")
map("n", [[<A-8>]], [[:lua require("bufferline").go_to_buffer(8)<CR>]], {"silent"}, "Go to buffer 8")
map("n", [[<A-9>]], [[:lua require("bufferline").go_to_buffer(9)<CR>]], {"silent"}, "Go to buffer 9")

map("n", [[<A-h>]], [[:lua require("config.nvim-bufferline").cycleBuf("BufferLineCyclePrev", "bp")<CR>]], {"silent"}, "Previous buffer")
map("n", [[<A-l>]], [[:lua require("config.nvim-bufferline").cycleBuf("BufferLineCycleNext", "bn")<CR>]], {"silent"}, "Next buffer")

map("n", [[<A-S-h>]], [[<CMD>BufferLineMovePrev<CR>]], {"silent"}, "Move current buffer to the left")
map("n", [[<A-S-l>]], [[<CMD>BufferLineMoveNext<CR>]], {"silent"}, "Move current buffer to the right")

end

return M
