-- File: selection
-- Author: iaso2h
-- Description: Selection Utilities
-- Version: 0.0.10
-- Last Modified: 2023-2-23
local api = vim.api
local cmd = vim.cmd
local fn  = vim.fn
local M   = {}


local jumpToEnd = function(selectEndPos)
    local endLineLen = #api.nvim_buf_get_lines(0, selectEndPos[1] - 1, selectEndPos[1], false)[1]
    -- In line-wise selection mode, the cursor is freely move horizontally
    -- in the last while maintain the selection unchanged. In this case,
    -- the cursor need to jump to where it located in the last line while
    -- line-wise selection is active.
    if selectEndPos[2] < endLineLen then
        -- Length of line have to minus 1 to convert to (1,0) based
        api.nvim_win_set_cursor(0, {selectEndPos[1], endLineLen - 1})
    else
        api.nvim_win_set_cursor(0, selectEndPos)
    end

end

local jumpToStart = function(selectStartPos)
    api.nvim_win_set_cursor(0, selectStartPos)
end


--- Jump to the corner position of the last previous selection area
---@param bias number 1 or -1. Set to 1 will make cursor jump to the closest
--- corner. Set to -1 to make cursor jump to furthest corner
function M.cornerSelection(bias) -- {{{
    local curPos         = api.nvim_win_get_cursor(0)
    local selectStartPos = api.nvim_buf_get_mark(0, "<")
    if selectStartPos[1] == 0 then return end  -- Sanity check
    local selectEndPos   = api.nvim_buf_get_mark(0, ">")
    if curPos[1] == selectStartPos[1] then
        api.nvim_win_set_cursor(0, selectEndPos)
        return
    elseif curPos[1] == selectEndPos[1] then
        api.nvim_win_set_cursor(0, selectStartPos)
        return
    end

    local closerToEnd = require("util").posDist(selectStartPos, curPos) < require("util").posDist(selectEndPos, curPos)
    if bias == 1 then
        if closerToEnd then
            jumpToStart(selectStartPos)
        else
            jumpToEnd(selectEndPos)
        end
    elseif bias == -1 then
        if closerToEnd then
            jumpToStart(selectStartPos)
        else
            jumpToEnd(selectEndPos)
        end
    end
end -- }}}


--- Substitute selected area
M.visualSub = function()
    ---@diagnostic disable-next-line: param-type-mismatch
    local str = string.gsub(M.getSelect("string", false), [[\]], [[\\]])
    api.nvim_feedkeys(string.format([[:s#\V%s]], str), "nt", false)
end


M.mirror = function()
    cmd("norm! gvd")
    local keyStr = "i" .. string.reverse(fn.getreg("-", 1)) .. t"<ESC>"
    api.nvim_feedkeys(keyStr, "tn", true)
end


--- Get the text of selected area
---@param returnType string Set to decide to return "string" or "list"
---@param exitToNormal boolean Set to decide to return in Normal mode in Neovim
---@return string|table Decided by returnType
M.getSelect = function(returnType, exitToNormal) -- {{{
    if returnType ~= "list" and returnType ~= "string" then
        return vim.notify("Not a supported string value", vim.log.levels.ERROR)
    end
    -- Not support blockwise visual mode
    local mode = fn.visualmode()
    if mode == "\22" then
        return vim.notify("Blockwise visual mode is not supported", vim.log.levels.WARN)
    end

    -- Return (1,0)-indexed line,col info
    local selectStart = api.nvim_buf_get_mark(0, "<")
    local selectEnd = api.nvim_buf_get_mark(0, ">")
    local lines = api.nvim_buf_get_lines(0, selectStart[1] - 1, selectEnd[1], false)

    if #lines == 0 then
        if returnType == "list" then
            return lines
        elseif returnType == "string" then
            return table.concat(lines, "\n")
        ---@diagnostic disable-next-line: missing-return
        end
    end

    -- Needed to remove the last character to make it match the visual selction
    if vim.o.selection == "exclusive" then selectEnd[2] = selectEnd[2] - 1 end
    if mode == "v" then
        lines[#lines] = lines[#lines]:sub(1, selectEnd[2] + 1)
        lines[1]      = lines[1]:sub(selectStart[2] + 1)
    end

    if exitToNormal then cmd("norm! " .. t"<Esc>") end

    if returnType == "list" then
        return lines
    elseif returnType == "string" then
        return table.concat(lines, "\n")
    ---@diagnostic disable-next-line: missing-return
    end
end -- }}}


--- Run selected code
M.runSelected = function()
    if not vim.bo.filetype == "lua" then
       return vim.notify("Only support in Lua file", vim.log.levels.WARN)
    end

    local lineStr = require("selection").getSelect("string", false)
    local vimMode = fn.visualmode()

    if vimMode == "\22" then
        return vim.notify("Blockwise visual mode is not supported", vim.log.levels.WARN)
    elseif vimMode == "V" then
        lineStr = string.gsub(lineStr, "\n", "")
    end

    vim.cmd("lua " .. lineStr)
end


return M

